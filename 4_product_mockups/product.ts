export class Product {
  id: number;
  name: string;
  description: string;
  pieces: number;
  price: number;
}
